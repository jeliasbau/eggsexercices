//
//  ViewController.swift
//  minEggDropper100_JEB
//
//  Created by Josep Elias on 16/01/2019.
//  Copyright © 2019 Josep Elias. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // necesitamos 4 variables
    var drops: Int = 0      // numero de intentos optimo
    var flats: Int = 100    // numero de pisos
    var i: Int = 1          // variable para hacer una progresión decreciente
    var dropsCount: Int = 0 // variable vara almacenar el valor del sumatorio
    
    // label para mostrar el valor óptimo máximo de intentos
    @IBOutlet var resultLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        minEggDropper100()
    }
    
    /* Para poder romper el mínimo de huevos y con un valor óptimo de intentos necesitamos encontrar un piso inicial sobre el que tirar el huevo e ir subiendo de piso (si no se rompe el huevo) o ir bajando de piso (en caso de romperse). El problema es que si partimos del piso 10 y vamos incrementando en 10 el numero de intentos máximos ira subiendo (10+1,10+2,10+3... el rango de intervalo sera creciente). Por tanto deberiamos encontrar un rango de un intervalo que sea constante si vamos subiendo de piso.*/
    
    /* En resumen, debemos encontrar un número que su progresión decreciente sume mas de 100. (por ejemplo el 20. 20+19+18+17... > 100) es decir, n+(n-1)+(n-2)...>100 */
    
    // función para calcular el valor óptimo máximo de intentos
    func minEggDropper100() {
        // cuando el valor supere el número de pisos saldra del bucle
        while dropsCount < flats {
            
            var contador = 1 // esta variable repetirá el bucle drops veces
            dropsCount = drops
            drops += 1
            
            // este bucle sirve para probar el sumatorio ( drops+(drops-1)+(drops-2)+... > 100 )
            while contador <= drops && dropsCount < flats {
                
                dropsCount = dropsCount + (drops - i) // sumatorio
                contador += 1
                i += 1
                }
            
            i = 1
            
        }
        // mostramos el valor drops en la vista con la resultLabel
        resultLabel.text = "El número de intentos máximos será de \(drops)"
        print ("numero de intentos es de \(drops)")
    }
    
}

// El resultado es 14. Si hacemos el sumatorio seria (14+13+12+11...>100) y es el primer número en pasar de 100.
// Si empezamos por el piso 14 la progresión es (14,27,39,50,60,69,77,84,90,95,99,100). Siempre haremos 14 intentos máximos aunque el huevo no se rompa y vayamos subiendo de piso. 27-14+1=14 / 39-27+2=14 /...


